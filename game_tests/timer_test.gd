extends Node2D

var active : bool = true

func _process(delta):
	
	if active:
		ECS.update()

	$Label.text = "%s" % $Spawner.get_component("timer").time_elapsed


func _on_Button_pressed():
	active = !active
