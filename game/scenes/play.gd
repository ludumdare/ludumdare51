extends Node2D

onready var root = $Root


enum PlayStates {
	PLAY_NONE,
	PLAY_IDLE,
	PLAY_RUN,
	PLAY_PAUSE,
	PLAY_RESUME,
	PLAY_INIT,
	PLAY_START,
	PLAY_STOP,
	PLAY_EXIT	
}


var play_state = PlayStates.PLAY_NONE
var next_state = PlayStates.PLAY_NONE
var state_changed : bool = false
var is_paused : bool = false

var wave_patterns = []
var wave_types = []
var wave_blocks = []

var wave_pattern_weight : int = 0

var score
var level
var waves
var wave
var rank
var shield

func _process(delta):
		
	match play_state:
		
		PlayStates.PLAY_EXIT:
			_state_exit()
		
		PlayStates.PLAY_IDLE:
			_state_idle()
			
		PlayStates.PLAY_INIT:
			_state_init()
		
		PlayStates.PLAY_PAUSE:
			_state_pause()
			
		PlayStates.PLAY_RESUME:
			_state_resume()
			
		PlayStates.PLAY_RUN:
			_state_run()
			
		PlayStates.PLAY_START:
			_state_start()
			
		PlayStates.PLAY_STOP:
			_state_stop()
			
	if Input.is_action_just_pressed("ui_cancel"):
		
		is_paused = !is_paused
		
		if is_paused:
			change_state(PlayStates.PLAY_PAUSE)
		else:
			change_state(PlayStates.PLAY_RESUME)
		
	if state_changed:
		state_changed = false
		play_state = next_state
		

func _ready():
	
	_load_wave_data()
	
	wave_blocks.append(Global.wave_type_block_25)
	wave_blocks.append(Global.wave_type_block_26)
	wave_blocks.append(Global.wave_type_block_27)
	wave_blocks.append(Global.wave_type_block_28)
	wave_blocks.append(Global.wave_type_block_29)
	wave_blocks.append(Global.wave_type_block_30)
	wave_blocks.append(Global.wave_type_block_31)
	wave_blocks.append(Global.wave_type_block_32)
	wave_blocks.append(Global.wave_type_block_33)
	wave_blocks.append(Global.wave_type_block_34)
	wave_blocks.append(Global.wave_type_block_35)
	wave_blocks.append(Global.wave_type_block_36)
	
	change_state(PlayStates.PLAY_INIT)
	
	Global.connect("spawn_trigger", self, "_on_spawner_trigger")
	Global.connect("block_hit", self, "_on_block_hit")
	Global.connect("ship_hit", self, "_on_ship_hit")
	
#	MusicManager.play("game2")
	

func _on_block_hit():
	pass
	
	
func _on_ship_hit():
	pass
	
	
func _on_spawner_trigger():
	
	Logger.info("_on_spawner_trigger")
	
	var _wave = Global.wave_entity.instance()
	var _patt = wave_patterns[rand_range(0,wave_pattern_weight)]
	var _block = wave_blocks[rand_range(0, wave_blocks.size())]
	
	var i = 0
	
	for p in "nmnnmnnnmnmn":
		
		if p != "n":
			
			var _type
			
			match p:
			
				"m":
					_type = Global.wave_type_meteor.instance()
					
				"b":
					_type = _block.instance()
					
				"s":
					_type = Global.wave_type_ship.instance()
					
			_wave.add_child(_type)
			_type.position = Vector2((80 * i) + 45 , 1)	
			
		i += 1
		
	root.add_child(_wave)
	
	ECS.rebuild()

func _state_exit():
	pass
	
	
func _state_idle():
	pass
	
	
func _state_init():
	score = 0
	level = 0
	wave = 0
	rank = 0
	change_state(PlayStates.PLAY_START)
	
	
func _state_none():
	pass
	
	
func _state_pause():
	change_state(PlayStates.PLAY_IDLE)
	
	
func _state_resume():
	change_state(PlayStates.PLAY_RUN)


func _state_run():
	ECS.update("run")


func _state_start():
	_on_spawner_trigger()
	change_state(PlayStates.PLAY_RUN)
	
	
func _state_stop():
	pass
	

func _load_wave_data():
	
	var _wave_data = Core.load_configuration("res://waves.cfg") as ConfigFile
	
	var _patterns = _wave_data.get_section_keys("wave_patterns")
	
	for _pattern in _patterns:
		var _wp = WavePattern.new()
		var _value = _wave_data.get_value("wave_patterns", _pattern)
		Logger.info(_value)
		var _values = _value.split(",")
		_wp.pattern = _values[0]
		_wp.weight = _values[1]
		for i in _wp.weight:
			wave_patterns.append(_wp)
		wave_pattern_weight += _wp.weight
		for _wps in wave_patterns:
			Logger.info("%s" % _wps.pattern)
			
	Logger.info("%s" % wave_pattern_weight)
	
	var _types = _wave_data.get_section_keys("wave_types")
	for _type in _types:
		var _wt = WaveType.new()
		var _value = _wave_data.get_value("wave_types", _type)
		Logger.info(_value)
	
func change_state(state):
	
	state_changed = true
	next_state = state
