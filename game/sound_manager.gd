extends Node2D

export (Array, Resource) var sounds


func play(name):
	
	var _player = AudioStreamPlayer.new() as AudioStreamPlayer
	_player.volume_db = -15
	
	add_child(_player)
	
	var _stream = _find_stream(name) as AudioStreamSample

	if (_stream):
		
		_player.stream = _stream
		_player.play()

		yield(_player, "finished")
		_player.queue_free()


func _find_stream(name):
	
	var _node =  get_node(name) as AudioStreamPlayer
	var _stream = _node.stream as AudioStreamSample
	return _stream
			
