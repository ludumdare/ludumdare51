class_name CollisionSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _collision = entity.get_component("collision") as CollisionComponent
	
	if (_collision.collision):
		
		var _name = _collision.collision.collider.name.replace("@","").to_lower()
		
		if (_name.begins_with("block")):
			ECS.remove_entity(_collision.collision.collider)
			Global.emit_signal("block_hit")
			
		if (_name.begins_with("ship")):
			ECS.remove_entity(_collision.collision.collider)			
			Global.emit_signal("ship_hit")
