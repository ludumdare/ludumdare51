class_name BlockCollisionSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _collision = entity.get_component("collision") as CollisionComponent
	
	if (_collision.collision):
		
		var _name = _collision.collision.collider.name.replace("@","").to_lower()
		
		if (_name.begins_with("shield")):
			Global.emit_signal("shield_hit")
			
		if (_name.begins_with("planet")):
			Global.emit_signal("planet_hit")
			ECS.remove_entity(entity)			
