class_name SpawnerSystem
extends System

func on_process_entity(entity : Entity, delta : float):

	var _spawner = entity.get_component("spawner") as SpawnerComponent
	
	if _spawner.spawn:
		
		_spawner.spawn = false	
		Logger.info("- spawning")
		Global.emit_signal("spawn_trigger")
