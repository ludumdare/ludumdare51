class_name MeteorBounceSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	var _collision = entity.get_component("collision") as CollisionComponent
	
	if (_collision.collision):
		
		var _name = _collision.collision.collider.name.replace("@","").to_lower()
#		Logger.info(_name)
		
		if (_name.begins_with("shield")):
			_move.direction = _move.direction.bounce(_collision.collision.normal)
			
		if (_name.begins_with("ball")):
			_move.direction = _move.direction.bounce(_collision.collision.normal)
			
		if (_name.begins_with("meteor")):
			_move.direction = _move.direction.bounce(_collision.collision.normal)
			
