class_name BounceSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _collision = entity.get_component("collision") as CollisionComponent
	
	if (_collision.collision):
		var _move = entity.get_component("move") as MoveComponent
		_move.direction = _move.direction.bounce(_collision.collision.normal)
