class_name SpawnTimerSystem
extends System

func on_process_entity(entity : Entity, delta : float):

	var _timer = entity.get_component("timer") as TimerComponent
	var _spawner = entity.get_component("spawner") as SpawnerComponent
	
	if _timer.time_elapsed > _timer.DURATION:
		_timer.time_elapsed = 0.0
		_spawner.spawn = true
