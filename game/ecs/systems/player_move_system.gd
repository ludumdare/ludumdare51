class_name PlayerMoveSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	
	entity.move_and_collide(_move.direction.normalized() * _move.SPEED * _move.SPEED_FACTOR * delta)
