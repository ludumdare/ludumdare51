class_name MoveSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	var _collision = entity.get_component("collision") as CollisionComponent
	
	_collision.collision = entity.move_and_collide(_move.direction * _move.SPEED * _move.SPEED_FACTOR * delta)
