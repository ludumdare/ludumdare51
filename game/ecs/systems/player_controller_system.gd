class_name PlayerControllerSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _move = entity.get_component("move") as MoveComponent
	
	var _inputx = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	_move.direction.x = _inputx * _move.SPEED

	var _inputy = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	_move.direction.y = _inputy * _move.SPEED

