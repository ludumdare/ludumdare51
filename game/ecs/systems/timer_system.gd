class_name TimerSystem
extends System

func on_process_entity(entity : Entity, delta : float):

	var _timer = entity.get_component("timer") as TimerComponent
	
	_timer.time_elapsed += delta
	
