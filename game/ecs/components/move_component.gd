class_name MoveComponent
extends Component

export var SPEED : float = 20
export var SPEED_FACTOR : float = 1.0

export var direction := Vector2.DOWN

