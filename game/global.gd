extends Node2D

signal block_hit
signal paddle_hit
signal spawn_trigger
signal ship_hit
signal shield_hit
signal planet_hit

var wave_entity = preload("res://game/ecs/entities/wave_entity.tscn")

var wave_type_block_25 = preload("res://game/wave_types/wave_type_block_25.tscn")
var wave_type_block_26 = preload("res://game/wave_types/wave_type_block_26.tscn")
var wave_type_block_27 = preload("res://game/wave_types/wave_type_block_27.tscn")
var wave_type_block_28 = preload("res://game/wave_types/wave_type_block_28.tscn")
var wave_type_block_29 = preload("res://game/wave_types/wave_type_block_29.tscn")
var wave_type_block_30 = preload("res://game/wave_types/wave_type_block_30.tscn")
var wave_type_block_31 = preload("res://game/wave_types/wave_type_block_31.tscn")
var wave_type_block_32 = preload("res://game/wave_types/wave_type_block_32.tscn")
var wave_type_block_33 = preload("res://game/wave_types/wave_type_block_33.tscn")
var wave_type_block_34 = preload("res://game/wave_types/wave_type_block_34.tscn")
var wave_type_block_35 = preload("res://game/wave_types/wave_type_block_35.tscn")
var wave_type_block_36 = preload("res://game/wave_types/wave_type_block_36.tscn")

var wave_type_ship = preload("res://game/wave_types/wave_type_ship.tscn")

var wave_type_meteor = preload("res://game/wave_types/wave_type_meteor.tscn")
